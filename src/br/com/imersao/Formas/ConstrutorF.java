package br.com.imersao.Formas;

import java.util.ArrayList;
import java.util.List;

public class ConstrutorF {
	
	public static Forma construir(List<Double> lados){
		
		if (lados.size() == 1) {
			return new Circulo(lados);
		}
		
		if(lados.size() == 2) {
			return new Quadrado(lados);
		}
		
		else if (lados.size() == 3){
			return new Triangulo(lados);
		}
		
		return null;
	}
	
}
