package br.com.imersao.Formas;

import java.util.ArrayList;
import java.util.List;

public class Circulo extends Forma{
	
	public Circulo(List<Double>diametro) {
		this.diametro = diametro;
	}

	private List<Double> diametro = new ArrayList<>();
	
	@Override
	public Double calcularArea() {
		
		
		return Math.pow(diametro.get(0), 2) * Math.PI;
	}

}
