package br.com.imersao.Formas;

import java.util.ArrayList;
import java.util.List;

public class App {

	public static void main(String[] args) {

		List<Double> l = new ArrayList<>();
		Console console = new Console();

		l = console.informarValores();
		Forma forma = ConstrutorF.construir(l);
		
		console.imprimir(forma.calcularArea());

	}

}
