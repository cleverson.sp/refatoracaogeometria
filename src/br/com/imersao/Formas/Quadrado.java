package br.com.imersao.Formas;

import java.util.ArrayList;
import java.util.List;

public class Quadrado extends Forma {

	List<Double> lados = new ArrayList<>();

	public Quadrado(List<Double> lados) {
		super();
		this.lados = lados;
	}

	@Override
	public Double calcularArea() {
		
		return lados.get(0) * lados.get(1) ;
	}
	
	
		
}
