package br.com.imersao.Formas;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Console {

private static Scanner scanner = new Scanner(System.in);

	public static List<Double> informarValores() {

		Double entrada = -1.0;
		
		List <Double> valores = new ArrayList<>();
		
		while (true) {
			System.out.println("Digite um lado ou digite zero para encerrar: ");

			String d = scanner.nextLine();
			entrada = Double.parseDouble(d);

			if (entrada > 0) {
				valores.add(entrada);
			} 
			else {
				break;
			}
		}

		return valores;

	}

	public static void imprimir(Object resultado) {

		System.out.println("O resultado é : " + resultado);

	}

}
