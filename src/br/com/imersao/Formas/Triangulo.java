package br.com.imersao.Formas;

import java.util.ArrayList;
import java.util.List;

public class Triangulo extends Forma {

	List<Double> lados = new ArrayList<>();

	public Triangulo(List<Double> lados) {
		this.lados = lados;
	}

	@Override
	public Double calcularArea() {

		if (lados.get(0) + lados.get(1) > lados.get(2) && (lados.get(0) + lados.get(2)) > lados.get(1)
				&& (lados.get(1) + lados.get(2)) > lados.get(0)) {
			double s = (lados.get(0) + lados.get(1) + lados.get(2)) / 2;
			return Math.sqrt(s * (s - lados.get(0)) * (s - lados.get(1)) * (s - lados.get(2)));
		} 
		else {

			throw new RuntimeException("Triângulo Inválido");
		}

	}

}
